Noviris
=======

# Installation du projet

	#récupération du code source et gestion des d�pendances

		-se placer dans le répertoire www puis lancer la commande
 		>  git clone https://bitbucket.org/kevchal972/noviris.git
		 
		   	username:kevchal972@hotmail.com
    		password:noviris123

 		pour récupérer le code source du projet

		-dans le répertoire du projet, récupérer les dépendances du projet
 		> composer update

	#installation de la base de donnée

		-vérifier le contenu du fichier app/config/config.yml et y saisir
		un login et un mot de passe valide pour la connexion avec MySQL 

		-se placer dans le répertoire du projet, et créer la base de données
		> php bin/console doctrine:database:create
	
		-les getters et setters des entités sont déjà présentes dans le code et l'étape suivante est facultative,
		il est possible de tous les regénérer avec la commande suivante:
		> php bin/console doctrine:generate:entities AppBundle
	
		-créer les tables, les champs et les relations de la base de donn�es avec la commande
		> php bin/console doctrine:schema:update --force

		L'installation des dépendances et de la base de données est terminée

	#Configuration des accès Admin
		La sécurisation de l'accès à l'interface d'administration est gérée avec le Bundle FOSUserBundle.
		
		- Pour créer un utilisateur, se placer dans une console et taper:
		> php bin/console fos:user:create test test@exemple.com password

		- Il faut ensuite lui rajouter les rôles ADMIN:
		> php bin/console fos:user:promote test ROLE_ADMIN

# Test de l'application

	# Front End
	
	-dans un premier temps, le front end comporte les champs suivants:
		+Nom
		+Prenom
		+Date de Naissance
	
	-si la case "permis" est cochée, deux listes déroulantes apparaissent
		+Modèle
		+Couleur

	-lors de la sélection du modèle une requête AJAX est envoyée pour récupérer
	les couleurs associées à la voiture, un sablier apparaît le temps que la requête s'exécute

	-lors de la validation, les donn�es sont persist�es dans la base de donn�es
 	

	# Back End
 	
	-l'architecture du back end est la suivante
		+User
		+--------Nom
		+--------Prenom
		+--------Date de Naissance
		+--------Permis de Conduire
		+--------Car
		+--------Couleur

		+Color
		+--------Nom

		+Car
		+--------Model
		+--------Color
			
	-Pour chacune de ces entr�es il est possible de les �diter, cr�er, supprimer et visualiser

# Piste d'am�lioration

	-Am�liorer l'interface c�t� client	