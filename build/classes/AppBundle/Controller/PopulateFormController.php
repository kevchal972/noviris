<?php
/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 22/05/2016
 * Time: 11:39
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;


class PopulateFormController extends Controller
{
public function getColorAction(Request $request)
{
    if($request->isXmlHttpRequest()) {
        $car_name = $request->get('car_name');
        $color = $this->get('color')->getColorNameByCarName($car_name);
        $response = new Response();
        $data = json_encode($color);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }
    else{
        return new Response('no ajax');
    }
}
}