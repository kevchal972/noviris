<?php
/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 22/05/2016
 * Time: 16:58
 */

namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;

class ValidateFormController extends Controller
{


    public function validateAction(Request $request){
        if($request->isXmlHttpRequest()) {

            $user = new user();
            $response = new Response();
            $last_name = $request->query->get('last_name');
            $first_name = $request->query->get('first_name');
            //$date_of_birth = $request->query->get('date_of_birth');
            $month = $request->query->get('month');
            $day = $request->query->get('day');
            $year = $request->query->get('year');
            $newMonth = $this->getMonth($month);
            $date_of_birth = $year.'-'.$newMonth.'-'.$day;
            var_dump($date_of_birth);
            $has_driver_license = $request->query->get('has_driver_license');
            $car_name = $request->query->get('car_id');
            $color_name = $request->query->get('color_id');

            $colorEntity = $this->get('color');
            $color_id = $colorEntity->getColorIdByName($color_name);

            $carEntity = $this->get('car');
            $car_id = $carEntity->getCarIdByName($car_name);

            $userInfo = array('lastname'=>$last_name,
                'firstname'=>$first_name,
                'date_of_birth'=>date($date_of_birth),
                'has_driver_license'=>$has_driver_license,
                'car_id'=>$car_id[0]['id'],
                'color_id'=>$color_id[0]['id']);
            $userEntity = $this->get('user');
            $userEntity->addUser($userInfo);
            var_dump($userInfo);
            return $response;
        }
        else{
            return new Response('no ajax');
        }
    }

    public function getMonth($month){
        if($month === 'Jan')
            return 1;
        if($month === 'Feb')
            return 2;
        if($month === 'Mar')
            return 3;
        if($month === 'Apr')
            return 4;
        if($month === 'May')
            return 5;
        if($month === 'Jun')
            return 6;
        if($month === 'Jul')
            return 7;
        if($month === 'Aug')
            return 8;
        if($month === 'Sep')
            return 9;
        if($month === 'Oct')
            return 10;
        if($month === 'Nov')
            return 11;
        if($month === 'Dec')
            return 12;
    }

}