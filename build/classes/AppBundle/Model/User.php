<?php


namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Query\ResultSetMapping as ResultSetMapping;
class User
{
    private $em;
    private $userMapping;
    public function __construct(EntityManager $entityManager){
        $this->em = $entityManager;
        $this->userMapping = new ResultSetMapping();
        $this->userMapping->addScalarResult('id','id')
            ->addScalarResult('firtname','firstname')
            ->addScalarResult('lastname','lastname')
            ->addScalarResult('date_of_birth','date_of_birth')
            ->addScalarResult('has_driver_license','has_driver_license')
            ->addScalarResult('car_id','car_id')
            ->addScalarResult('color_id','color_id');
    }

    public function addUser($user){
        $insert = 'INSERT INTO user (firstname,lastname,date_of_birth,has_driver_license,car_id_id,color_id_id) VALUES ("'.$user['firstname'].'","'.$user['lastname'].'","'.$user['date_of_birth'].'","'.$user['has_driver_license'].'","'.$user['car_id'].'","'.$user['color_id'].'")';
        $this->em->createNativeQuery($insert,$this->userMapping)->execute();
    }
}