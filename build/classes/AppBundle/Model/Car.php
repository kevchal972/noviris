<?php

/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 21/05/2016
 * Time: 13:53
 */

namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;

class Car
{
    private $em;
    private $carMapping;
    public function __construct(EntityManager $entityManager){
        $this->em = $entityManager;
        $this->carMapping = new ResultSetMapping();
        $this->carMapping->addScalarResult('id','id')
            ->addScalarResult('name','name');
    }

    public function getCarName(){
        $result = array();
        $select = 'SELECT name FROM car ';
        $models = $this->em->createNativeQuery($select,$this->carMapping)->execute();
        $i=0;
        foreach($models as $model){
            $result[$model['name']] = $i;
            $i++;
        }
        return $result;
    }

    public function getCarIdByName($car_id){
        $select = 'SELECT id FROM car WHERE name='."'".$car_id."'";
        return $this->em->createNativeQuery($select,$this->carMapping)->execute();
    }

}