<?php

/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 21/05/2016
 * Time: 12:51
 */
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Extension\Field\Type;
use AppBundle\Entity\Color;
class CarAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('color');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('color');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
                   ->addIdentifier('color')
                   ->add('_action', 'actions', array(
                                                'actions' => array(
                                                    'edit' => array(),
                                                    'delete' => array(),
                                                    )
                        )
                   );
    }
}
