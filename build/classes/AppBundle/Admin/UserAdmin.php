<?php
/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 22/05/2016
 * Time: 09:10
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Extension\Field\Type;

class UserAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)//add
    {
        $formMapper
            ->add('firstname', 'text')
            ->add('lastname','text')
            ->add('date_of_birth','date')
            ->add('has_driver_license')
            ->add('color_id')
            ->add('car_id');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstname')
            ->add('lastname')//add
            ->add('date_of_birth')
            ->add('has_driver_license')
            ->add('color_id')
            ->add('car_id');
    }

    protected function configureListFields(ListMapper $listMapper)//list
    {
        $listMapper->addIdentifier('firstname')
                   ->addIdentifier('lastname')
                   ->addIdentifier('date_of_birth')
                   ->add('has_driver_license')
                   ->addIdentifier('car_id')
                   ->addIdentifier('color_id')
                   ->add('_action', 'actions', array(
                                                'actions' => array(
                                                    'edit' => array(),
                                                    'delete' => array(),
                                                    )
                                                )
                    );
    }

}