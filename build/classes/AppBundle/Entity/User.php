<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Color")
     * @ORM\JoinColumn(nullable=false)
     */
    private $color_id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Car")
     * @ORM\JoinColumn(nullable=false)
     */
    private $car_id;


    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;


    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var Date
     *
     * @ORM\Column(name="date_of_birth", type="date")
     */
    private $date_of_birth;


    /**
     * @var boolean
     *
     * @ORM\Column(name="has_driver_license", type="boolean")
     */
    private $has_driver_license;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setDateOfBirth($date_of_birth)
    {
        $this->date_of_birth = $date_of_birth;
    }

    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }

    public function setHasDriverLicense($bool)
    {
        $this->has_driver_license = $bool;
    }

    public function getHasDriverLicense()
    {
        return $this->has_driver_license;
    }

    public function setCarId($car_id)
    {
        $this->car_id = $car_id;
    }

    public function getCarId()
    {
        return $this->car_id;
    }

    public function setColorId($color_id)
    {
        $this->color_id = $color_id;
    }

    public function getColorId()
    {
        return $this->color_id;
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\Color $user
     *
     * @return User
     */
    public function setUser(\AppBundle\Entity\Color $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Color
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set color
     *
     * @param \AppBundle\Entity\Color $color
     *
     * @return User
     */
    public function setColor(\AppBundle\Entity\Color $color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Set car
     *
     * @param \AppBundle\Entity\Car $car
     *
     * @return User
     */
    public function setCar(\AppBundle\Entity\Car $car)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get color
     *
     * @return \AppBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Get car
     *
     * @return \AppBundle\Entity\Car
     */
    public function getCar()
    {
        return $this->car;
    }
}
