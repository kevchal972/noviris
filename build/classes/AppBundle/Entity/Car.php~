<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DoctrineCommonCollectionsArrayCollection;
/**
 * Car
 *
 * @ORM\Table(name="car")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CarRepository")
 */
class Car
{

    /**
     * @var ArrayCollection Color $color
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Color", inversedBy="car", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="car_has_color",
     *   joinColumns={@ORM\JoinColumn(name="id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="id", referencedColumnName="id")}
     * )
     */
    private $color;
    
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Car
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->car = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add car
     *
     * @param \AppBundle\Entity\User $car
     *
     * @return Car
     */
    public function addCar(\AppBundle\Entity\User $car)
    {
        $this->car[] = $car;

        return $this;
    }

    /**
     * Remove car
     *
     * @param \AppBundle\Entity\User $car
     */
    public function removeCar(\AppBundle\Entity\User $car)
    {
        $this->car->removeElement($car);
    }

    /**
     * Get car
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Add carId
     *
     * @param \AppBundle\Entity\User $carId
     *
     * @return Car
     */
    public function addCarId(\AppBundle\Entity\User $carId)
    {
        $this->car_id[] = $carId;

        return $this;
    }

    /**
     * Remove carId
     *
     * @param \AppBundle\Entity\User $carId
     */
    public function removeCarId(\AppBundle\Entity\User $carId)
    {
        $this->car_id->removeElement($carId);
    }

    /**
     * Get carId
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarId()
    {
        return $this->car_id;
    }
}
