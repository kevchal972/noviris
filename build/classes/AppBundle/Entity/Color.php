<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Color
 *
 * @ORM\Table(name="color")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ColorRepository")
 */
class Color
{

    /**
     * @var ArrayCollection Produit $clients
     *
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="Car", mappedBy="color", cascade={"all"})
     */
    private $car;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Color
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->color = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add color
     *
     * @param \AppBundle\Entity\User $color
     *
     * @return Color
     */
    public function addColor(\AppBundle\Entity\User $color)
    {
        $this->color[] = $color;

        return $this;
    }

    /**
     * Remove color
     *
     * @param \AppBundle\Entity\User $color
     */
    public function removeColor(\AppBundle\Entity\User $color)
    {
        $this->color->removeElement($color);
    }

    /**
     * Get color
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add colorId
     *
     * @param \AppBundle\Entity\User $colorId
     *
     * @return Color
     */
    public function addColorId(\AppBundle\Entity\User $colorId)
    {
        $this->color_id[] = $colorId;

        return $this;
    }

    /**
     * Remove colorId
     *
     * @param \AppBundle\Entity\User $colorId
     */
    public function removeColorId(\AppBundle\Entity\User $colorId)
    {
        $this->color_id->removeElement($colorId);
    }

    /**
     * Get colorId
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColorId()
    {
        return $this->color_id;
    }

    /**
     * Add car
     *
     * @param \AppBundle\Entity\Car $car
     *
     * @return Color
     */
    public function addCar(\AppBundle\Entity\Car $car)
    {
        $this->car[] = $car;

        return $this;
    }

    /**
     * Remove car
     *
     * @param \AppBundle\Entity\Car $car
     */
    public function removeCar(\AppBundle\Entity\Car $car)
    {
        $this->car->removeElement($car);
    }

    /**
     * Get car
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCar()
    {
        return $this->car;
    }

    public function __toString(){
        return $this->name;
    }
}
