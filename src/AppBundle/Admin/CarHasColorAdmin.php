<?php
/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 21/05/2016
 * Time: 19:20
 */

namespace AppBundle\Admin;


namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\Extension\Field\Type;

class CarHasColorAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('color_id','int')
            ->add('car_id','int');
    }
}
