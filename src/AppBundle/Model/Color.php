<?php
/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 21/05/2016
 * Time: 13:57
 */

namespace AppBundle\Model;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Query\ResultSetMapping as ResultSetMapping;
class Color
{

    private $em;
    private $colorMapping;
    public function __construct(EntityManager $entityManager){
        $this->em = $entityManager;
        $this->colorMapping = new ResultSetMapping();
        $this->colorMapping->addScalarResult('id','id')
            ->addScalarResult('name','name');
    }

    public function getColorName(){
        $select = 'SELECT name FROM color ';
        return $this->em->createNativeQuery($select,$this->colorMapping)->execute();
    }

    public function getColorIdByName($color_name){
        $select = 'SELECT id FROM color WHERE name='."'".$color_name."'";
        return $this->em->createNativeQuery($select,$this->colorMapping)->execute();
    }

    public function getColorNameByCarName($car_name){
        $select = 'SELECT co.name FROM color co JOIN car_has_color chc ON co.id = chc.color_id JOIN car ca ON ca.id = chc.car_id WHERE ca.name="'.$car_name.'"';
        return $this->em->createNativeQuery($select,$this->colorMapping)->execute();
    }
}