<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoicesType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\JsonResponse;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // create a task and give it some dummy data for this example

        $user = new User();
        $car = $this->get('car');
        $color = $this->get('color');
        $models = $car->getCarName();
        $form = $this->createFormBuilder($user)
            ->add('lastname', TextType::class, array('label'=>'Nom'))
            ->add('firstname', TextType::class, array('label'=>'Prenom'))
            ->add('date_of_birth','date', array(/*'format' => 'yyyy-MM-dd','label' => 'Date de Naissance',*/
                'years' => range(date('Y')-100, date('Y')-18),
            ))
            //l'utilisateur doit �tre majeur
            ->add('has_driver_license', CheckboxType::class, array( 'label'=>'Avez vous le permis ?', 'required' => false))
            ->add('car_id', 'choice', array('label_attr' => array('id'=>'model-label'),'label'=>'Model', 'choices' => $models ))
            ->add('color_id', ChoiceType::class, array('label_attr' => array('id'=>'color-label'),'label'=>'Couleur'))
            ->add('save', SubmitType::class, array('label' => 'Valider'))
            ->getForm();

        if($form->handleRequest($request)) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('default/default.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
