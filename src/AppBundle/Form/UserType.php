<?php

/**
 * Created by PhpStorm.
 * User: Kev
 * Date: 21/05/2016
 * Time: 10:15
 */
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Nom', TextType::class)
            ->add('Pr�nom', TextType::class)
            ->add('Date de Naissance', DateType::DEFAULT_FORMAT)
            ->add('save', SubmitType::class, array('label' => 'Create Task'))
            ->getForm();
    }

}